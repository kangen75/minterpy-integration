"""
This module is part of `minterpy-integration`.

It contains the data classes 

`IntegratorSetup`:
    Stroage of all meta data of the integration, incl. points and weights for the quadrature rule
`Integrator`:
    Wrappes around the `IntegratorSetup` and provides the method `Integrator.integrate`, which performs the quadrature rule on a given function.
"""

import attr
import numpy as np
import minterpy as mp


from .utils import _check_spatial_dimension,_check_poly_degree,_check_lp_degree
from .MonomialIntegrator import _monomial_integrate

__all__ = ["Integrator","IntegratorSetup"]


@attr.s(frozen=True, slots=True)
class IntegratorSetup:
    # obligatory arguments
    spatial_dimension = attr.ib()
    poly_degree = attr.ib()
    lp_degree = attr.ib()

    # optional arguments
    weight_function = attr.ib(kw_only = True, default = 'legendre')
    point_kind = attr.ib(kw_only=True,default = 'leja')
    use_data = attr.ib(kw_only=True, default = False)

    # internally builded attributes
    multi_index = attr.ib(init=False,repr=False)
    grid = attr.ib(init=False,repr=False)
    poly = attr.ib(init=False,repr=False)
    unisolvent_nodes = attr.ib(init=False,repr=False)
    transformation_matrix = attr.ib(init=False,repr=False)
    integrated_canon_monomials = attr.ib(init=False,repr=False)
    nodes = attr.ib(init=False,repr=False)
    weights = attr.ib(init = False,repr=False)
    
    
    @lp_degree.validator
    def __check_lp_degree(self,attribute,value):
        _check_lp_degree(value)
    
    @poly_degree.validator
    def __poly_degree_default(self,attribute,value):
        _check_poly_degree(value)
    
    @spatial_dimension.validator
    def __spatial_dimension_default(self,attribute,value):
        _check_lp_degree(value)
        
    @multi_index.default
    def __build_multi_index(self):
        return mp.MultiIndexSet.from_degree(self.spatial_dimension,self.poly_degree,self.lp_degree)
        
    @grid.default
    def __build_grid(self):
        return mp.Grid.from_generator(self.multi_index)
        
    @poly.default
    def __build_poly(self):
        return mp.LagrangePolynomial(self.multi_index,grid=self.grid)
    
    @unisolvent_nodes.default
    def __build_unisolvent_nodes(self):
        
        return self.poly.grid.unisolvent_nodes
    
    @transformation_matrix.default
    def __build_transformation_matrix(self):
        trans = mp.get_transformation(self.poly,mp.CanonicalPolynomial)
        return trans.transformation_operator.array_repr_full
    
    @integrated_canon_monomials.default
    def __build_integrated_canon_monomials(self):
        return _monomial_integrate(self.multi_index.exponents.T,self.weight_function)
    
    @nodes.default
    def __build_integ_nodes(self):
        return self.unisolvent_nodes
    
    @weights.default
    def __build_weights(self):
        return np.dot(self.transformation_matrix.T,self.integrated_canon_monomials)
    

@attr.s(frozen=True, slots=True)
class Integrator:
    # obligatory arguments
    spatial_dimension = attr.ib()
    poly_degree = attr.ib()
    lp_degree = attr.ib()

    # optional arguments
    weight_function = attr.ib(kw_only = True, default = 'legendre')
    point_kind = attr.ib(kw_only=True,default = 'leja')
    use_data = attr.ib(kw_only=True, default = False)
    
    # computed internal attributes
    setup = attr.ib(init=False, repr=False)
    
    @setup.default
    def __build_setup(self):
        return IntegratorSetup(self.spatial_dimension,self.poly_degree,self.lp_degree,weight_function = self.weight_function,point_kind = self.point_kind,use_data = self.use_data)
    
    def integrate(self,func):
        return np.dot(func(self.setup.nodes.T),self.setup.weights)
