"""
This module is part of `minterpy-integration`.

It contains functions to perform the monomial integration for a given set of exponents, i.e. multi index sets, and a given weight function.
"""
import numpy as np


def _monomial_integrate(gamma,weight_function = 'legendre'):
    """
    Integrate monomials in canonical base on [-1,1]^d for a given weight function.
    """
    #print("weight_function",weight_function)
    if weight_function=='legendre':
        return monomial_integrate_leg(gamma)
    else:
        raise ValueError("There is no weight function called <%>"%weight_function)

def monomial_integrate_leg(gamma):
    """
    Integrate monomials in canonical base on [-1,1]^d for the Legendre weight function, i.e. w(x) = 1.
    """
    #print("!!!! Warning: integration on odd monomials are set zero... check if this is right!")
    case = np.mod(gamma,2)==0
    temp_gamma = gamma[case]
    temp_integ = 2/(temp_gamma+1)
    res = np.zeros(gamma.shape)
    res[case] = temp_integ
    return res.prod(axis = 0)