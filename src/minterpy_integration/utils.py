"""
This module is part of `minterpy-integration`.

It contains several utility function.
"""

def assert_type(value,type_,name=None):
    """ asserts a certain type of the input.
    
    """
    if type(value) is not type_: raise TypeError(f"The type of {(name if name is not None else 'the input value')} needs to be an {type_}! But {type(value)} is given.")
        

def assert_subtype(value,type_,name=None):
    """ asserts input value is subtype of a certain type.
    
    """
    if isinstance(value,type_): raise TypeError(f"The {(name if name is not None else 'the input value')} needs to be a subtype of {type_}! But {type(value)} is given.")
        

def assert_positive(value,name=None):
    """ Asserts that the input value is positive.
    
    .. todo:: 
        - make this avialable for numpy arrays as well.
    
    """
    assert value>=0, f"The value of {(name if name is not None else 'the input')} needs to be positive."
        
        
def _check_spatial_dimension(spatial_dimension):
    """Validator for spatial_dimension."""
    assert_type(spatial_dimension, int, 'spatial_dimension')
    assert_positive(spatial_dimension, 'spatial_dimension')
    
    
def _check_poly_degree(poly_degree):
    """Validator for poly_degree."""
    assert_type(poly_degree, int, 'poly_degree')
    assert_positive(poly_degree, 'poly_degree')
    
def _check_lp_degree(lp_degree):
    """Validator for lp_degree."""
    assert_subtype(lp_degree,float, 'lp_degree')
    assert_positive(lp_degree, 'lp_degree')