# minterpy-integration
[![Documentation Status][rtd-badge]][rtd-link]
[![Code style: black][black-badge]][black-link]

[![PyPI version][pypi-version]][pypi-link]
[![Conda-Forge][conda-badge]][conda-link]
[![PyPI platforms][pypi-platforms]][pypi-link]




[black-badge]:              https://img.shields.io/badge/code%20style-black-000000.svg
[black-link]:               https://github.com/psf/black
[conda-badge]:              https://img.shields.io/conda/vn/conda-forge/minterpy-integration
[conda-link]:               https://github.com/conda-forge/minterpy-integration-feedstock
[pypi-link]:                https://pypi.org/project/minterpy-integration/
[pypi-platforms]:           https://img.shields.io/pypi/pyversions/minterpy-integration
[pypi-version]:             https://badge.fury.io/py/minterpy-integration.svg
[rtd-badge]:                https://readthedocs.org/projects/minterpy-integration/badge/?version=latest
[rtd-link]:                 https://minterpy-integration.readthedocs.io/en/latest/?badge=latest


## Installation
There is one main required python package, which can not be installed using `pip`: `minterpy`. Therefore, we need to install `minterpy` from source. First clone the minterpy repository

```bash 
git clone https://gitlab.hzdr.de/interpol/minterpy.git
```

Then follow the install instructions in `minterpy/README.md`. It is highly recommended to install `minterpy` into a virtual environment. Nevertheless, `minterpy-integration` needs to be installed into the same python scope, where `minterpy` lives. To actually build `minterpy-integration`, just clone the repository

```bash
git clone https://gitlab.hzdr.de/interpol/minterpy-integration.git
```

Then go to `minterpy-integration/`, remember to activate your virtual environment, and hit

```bash
pip install -e .
```

This allowes pip to install all dependencies, beside `minterpy`. After the install routine finished, you may run 

```bash
pytest
```

in order to check the validity of the installation (for now, it just checks if the package is importable)

## Quickstart
Say we want to integrate a polynomial in 3D space:

```python
import numpy as np

def f(x):
    return -2*x[0]**4 + 4*x[1]**5 + 3*x[2]*x[0]**6 + 10*x[1]**9
```

which is polynomial degree $n=9$ for $l_p=1$.

First we need initialize an Integrator

```python
import minterpy_integration as mint
integrator = mint.Integrator(spatial_dimension = 3, 
                             poly_degree = 5,
                             lp_degree = 1)
```
Then we can use this integrator to integrate the polynomial given above

```python
res = integrator.integrate(f)
```

We may check the result against well established integrators (this needs scipy to be installed)

```python
from scipy.integrate import tplquad

def f_wrapper(x,y,z):
    return f([x,y,z])

res_quad = tplquad(f_wrapper, -1, 1, lambda x: -1, lambda x: 1,lambda x, y: -1, lambda x, y: 1)
```

Hopefully, they are close:

```python
np.isclose(res,res_quad[0]) # true
```


## Structure of the project

The repository is structured as

```
├── LICENSE                             # license file
├── MANIFEST.in                         # exact versions of the requirements
├── README.md                           # global meta informations
├── docs/                               # documentation directory
├── notebooks/                          # notebooks to work with the package
├── references/                         # assets (pdfs, external plots)
├── setup.cfg                           # config file for the installation
├── setup.py                            # entry file for pip
├── src                                 # python source of the package
│   ├── minterpy_integration            # source files
│   │   ├── MonomialIntegrator.py       # monomial integrations
│   │   ├── __init__.py                 # entry file for the package
│   │   ├── _version.py                 # versioneerer
│   │   ├── integrator.py               # intergrator classes
│   │   ├── py.typed                    # typing in python
│   │   └── utils.py                    # utility functions
│   └── minterpy_integration.egg-info   # build information
└── tests                               # test directory
    └── test_package.py                 # test if package is importable
```